# README #

This is README file for jQuery based plugin got Responsive Tabs.

### What is this repository for? ###

* Repository contains an example which is created using jQuery based Responsive Tabs plugin.
* Version 1.0
* Demo : http://jqresponsivetabs.nikhilmaheshwari.com/

### How do I get set up? ###

* lib/jquery.responsive-tabs.js : Plugin file for Responsive Tabs.
* css/responsive-tabs.css : CSS code for Responsive Tabs.
* js/main.js : Application's Main JS file to start the plugin.
* img/menu.png : Icon for responsive menu.

### Contribution guidelines ###

* Author : Nikhil Maheshwari

### Who do I talk to? ###

* http://www.nikhilmaheshwari.in
* http://www.nikhilmaheshwari.com
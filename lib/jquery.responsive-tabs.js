(function ($) {

    $.fn.responsiveTabs = function ( options ) {
        
        var settings = $.extend({
            activeTab    : 0,
            menuIconPath     : "img/menu.png",
            tabsMenuNarrowClass : 'tabs_menu_narrow',
            tabsMenuWideClass   : 'tabs_menu_wide'
        }, options);

        this.each(function (index, element) {

            var tabGroupId = $(this).attr("id");

            $('#' + tabGroupId + ' div').each(function (index, element) {
                $(this).attr('id', 'tab_content_' + index);
                $(this).addClass('tab_content');
                if (index == settings.activeTab) {
                    $(this).addClass('active');
                }
            });

            var tabContent = $('#' + tabGroupId + ' .tab_content');
            var tabsMenuWide = $(document.createElement("div"));
            var tabsMenuNarrow = $(document.createElement("div"));

            tabsMenuWide.addClass(settings.tabsMenuWideClass);

            tabsMenuNarrow.on('click', function () {
                $.fn.showHideMenu(tabGroupId);
            });

            tabsMenuNarrow.addClass(settings.tabsMenuNarrowClass);
            tabsMenuNarrow.html("<div class='left_div'><p><b>Menu</b></p></div><div class='right_div'><img src='" + settings.menuIconPath +"'/></div>");
            tabsMenuWide.append(tabsMenuNarrow);

            var ul = $(document.createElement("ul"));

            for (var i = 0; i < tabContent.length; i++) {
                var isActive = $(tabContent[i]).hasClass('active');
                var tabNumber = $(tabContent[i]).attr("id")[$(tabContent[i]).attr("id").lastIndexOf("_") + 1];
                var tabTitle = $(tabContent[i]).children('h1').html();
                var li = $(document.createElement("li"));
                var a = $(document.createElement("a"));

                a.attr('id', 'tab_' + tabNumber);
                a.attr('href', 'javascript:void(0)');
                a.html(tabTitle);

                if (isActive) {
                    a.addClass('active');
                }

                a.on('click', function () {
                    $.fn.createTabs(this, tabGroupId);
                });

                li.append(a);
                ul.append(li);
            }

            tabsMenuWide.append(ul);
            $(this).prepend(tabsMenuWide);
        });

        $.fn.createTabs = function (elem, tabGroupId) {
            var tabNumber = $(elem).attr("id")[$(elem).attr("id").lastIndexOf("_") + 1];

            $('#' + tabGroupId + ' .tabs_menu_wide a').removeClass('active');
            $(elem).addClass('active');

            $('#' + tabGroupId + ' .tab_content').each(function (index, element) {
                if ($(this).attr('id') == 'tab_content_' + tabNumber) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        }

        $.fn.showHideMenu = function (tabGroupId) {
            $('#' + tabGroupId + ' .tabs_menu_narrow').hide();
            $('#' + tabGroupId + ' .tabs_menu_wide ul li a').show();
            $('#' + tabGroupId + ' .tabs_menu_wide').bind('mouseleave', function () {
                $('#' + tabGroupId + ' .tabs_menu_narrow').show();
                $('#' + tabGroupId + ' .tabs_menu_wide li a').hide();
            });
        }

        $(window).resize(function () {
            if ($(window).width() >= 500) {
                $('.responsive_tabs').hide();
                $('.tabs_menu_wide li a').show();
            } else {
                $('.tabs_menu_wide').unbind('mouseleave');
                $('.responsive_tabs').show();
                $('.tabs_menu_wide li a').hide();
            }
        });

    }

}(jQuery));